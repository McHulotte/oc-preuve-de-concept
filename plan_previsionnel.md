# Projet 7 : Développez une preuve de concept

## Plan prévisionnel de travail

**Objectif : Améliorer les résultats du projet 5 Catégorisez Automatiquement des questions en utilisant une meilleure technique d'embedding : Text Embedding ADA 002**

Le travail sera réalisé avec les mêmes données que le projet 5.

---


### 1. Recherches 
- [x] Analyser les nouvelles techniques d'embedding
- [x] Choisir un ou plusieurs algorithmes récent montrant des bons résultats
### 2. Mise en place
- [x] Récupérer les données et le travail effectués du début du projet 5
- [x] Se renseigner sur les différentes optimisations possibles pour les techniques d'embedding
- [x] Mettre en place les nouvelles techniques d'embeddings
- [x] Optimiser l'embedding
### 3. Analyse des résultats
- [x] Comparer les différentes techniques
- [x] Interpréter les résultats
  

---

### Sources bibliographiqes

Types de word embeddings et leurs applications : https://www.kdnuggets.com/2021/11/guide-word-embedding-techniques-nlp.html

Benchmark Embeddings : https://paperswithcode.com/sota/zero-shot-text-search-on-beir

OpenAI embedder : https://platform.openai.com/docs/guides/embeddings/what-are-embeddings

GPT Sentence Embeddings for Semantic Search Abstract : https://arxiv.org/pdf/2202.08904v5.pdf

SGPT Github:  (1er du Benchmark) : https://github.com/muennighoff/sgpt



